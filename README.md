# WebJive Features

Issues in this project are meant to be brief descriptions of extensions and improvements that we plan to make to WebJive. Milestones have been used to represent "visions" that we have for WebJive, at different points in the future.
"We" means the team in MaxIV that created WebJive and people in the SKA project. 
I say people because there are several SKA stakeholders involved, some are developers (now belonging to a specific SKA development team), other ones are Product Owners and Product Managers.

26/June/2019: At the moment we, SKA people, have not committed yet to implement solutions for the limitations/extensions described in these issues (we completed a first round of interviews a few days ago). We will plan what to implement during the next week.

The issues in the board are ranked according to my own opinion on their urgency (basically how costly it would be for SKA to delay that issue).


2019-09-12: This file describes some of the findings derived from several interviews held in July-August 2019 with members of SKA teams.

[SKA-TEL-SW-0000002-C_WebJiveRoadmapDocument.pdf](/uploads/95b6d5a0ac452368977f6686c6d8b2d8/SKA-TEL-SW-0000002-C_WebJiveRoadmapDocument.pdf)

Giorgio Brajnik

